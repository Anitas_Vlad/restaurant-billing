package defaultPackage;

import database.dao.FoodDao;
import database.entity.Food;

import java.util.ArrayList;

public class Kitchen {
    private FoodDao foodDao;

    public ArrayList<Food> cookFood(ArrayList<Integer> desiredFood) {

        ArrayList<Food> foodToBeCooked = new ArrayList<>();

        for (Integer integer : desiredFood) {

                switch (integer) {
                    case 1 -> foodToBeCooked.add(foodDao.getFood(1));
                    case 2 -> foodToBeCooked.add(foodDao.getFood(2));
                    case 3 -> foodToBeCooked.add(foodDao.getFood(3));
                    case 4 -> foodToBeCooked.add(foodDao.getFood(4));
                    case 5 -> foodToBeCooked.add(foodDao.getFood(5));
                    case 6 -> foodToBeCooked.add(foodDao.getFood(6));
                    case 7 -> foodToBeCooked.add(foodDao.getFood(7));
                    case 8 -> foodToBeCooked.add(foodDao.getFood(8));
                    case 9 -> foodToBeCooked.add(foodDao.getFood(9));
                    case 10 -> foodToBeCooked.add(foodDao.getFood(10));
                    case 11 -> foodToBeCooked.add(foodDao.getFood(11));
                    case 12 -> foodToBeCooked.add(foodDao.getFood(12));
                    case default -> foodToBeCooked.add(null);
                }
        }
        return foodToBeCooked;
    }
}
