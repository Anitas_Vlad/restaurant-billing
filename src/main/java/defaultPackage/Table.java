package defaultPackage;

import database.entity.Food;
import exceptions.CannotLeaveTheRestaurant;
import exceptions.CannotPay;

import java.util.ArrayList;

public class Table {
    private Integer number;
    private boolean isOccupied;
    private Person currentPerson;
    private IO tableIO;

    private final Bill bill;

    public Table(Integer number, Restaurant restaurant) {
        this.number = number;
        this.isOccupied = false;
        this.bill = new Bill(restaurant);
        this.tableIO = new IO();
    }

    public Bill getBill() {
        return bill;
    }

    public void setOccupied(boolean occupied) {
        this.isOccupied = occupied;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setCurrentPerson(Person currentPerson) {
        this.currentPerson = currentPerson;
    }

    public Integer getNumber() {
        return number;
    }

    public Person getCurrentPerson() {
        return currentPerson;
    }

    public void occupy(Person person){
        this.setOccupied(true);
        this.setCurrentPerson(person);
    }

    public void ejectPerson(Restaurant restaurant) throws CannotLeaveTheRestaurant { //TODO Would like not needing to add parameters here. same goes for Leaving restaurant.
        restaurant.ejectPersonFromRestaurant(currentPerson);
    }

    public void addToOrder() {
        Integer foodId = tableIO.readInteger();
        bill.addToBill(foodId);
    }

    public ArrayList<Food> orderFood() {
        return this.bill.sendOrderToKitchen();
    }

    public void askForBill() {
        this.bill.printBillDetails();
    }

    public void payBill() throws CannotPay {
        currentPerson.pay(bill);
        bill.resetBill();
    }
}
