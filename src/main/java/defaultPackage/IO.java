package defaultPackage;

import java.util.Scanner;

public class IO {
    private Scanner scanner;

    public IO(){
        this.scanner = new Scanner(System.in);
    }

    public String readString(){
        return scanner.nextLine();
    }

    public Integer readInteger(){
        return scanner.nextInt();
    }

    public void printText(String text){
        System.out.println(text);
    }
}
