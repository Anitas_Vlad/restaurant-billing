package defaultPackage;

import exceptions.CannotEnterTheRestaurant;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws CannotEnterTheRestaurant {

//        FoodDao foodDao = new FoodDao();
//        System.out.println(foodDao.showFoodMenu());

        Kitchen alohaKitchen = new Kitchen();
        Restaurant alohaRestaurant = new Restaurant("Aloha", alohaKitchen);

        ArrayList<Table> alohaTables = new ArrayList<>();
        Table table1 = new Table(1, alohaRestaurant);
        Table table2 = new Table(2, alohaRestaurant);
        Table table3 = new Table(3, alohaRestaurant);
        Table table4 = new Table(4, alohaRestaurant);
        Table table5 = new Table(5, alohaRestaurant);
        alohaTables.add(table1);
        alohaTables.add(table2);
        alohaTables.add(table3);
        alohaTables.add(table4);
        alohaTables.add(table5);
        alohaRestaurant.setTables(alohaTables);

        Person vlad = new Person("Vlad", 100d);
        Person alex = new Person("Alex", 200d);
        vlad.enterRestaurant(alohaRestaurant);
        alex.enterRestaurant(alohaRestaurant);
        vlad.sitAtTableV2(alohaRestaurant, table1); // works based on parameter
        alex.sitAtTable(alohaRestaurant); // works based on Scanner.

        alohaRestaurant.showMenu();
//        alohaRestaurant.showAvailableTables();

//        HibernateUtil.shutdown();

        //TODO Bill -> Order / A way to simplify the project
    }
}
