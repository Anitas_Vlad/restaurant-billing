package defaultPackage;

import database.dao.OrderDao;
import database.entity.Food;
import database.entity.Order;

import java.util.ArrayList;

public class Bill {

    private ArrayList<Food> orderedFoods; //TODO THIS
    private Restaurant restaurant;
    private double total; //TODO THIS
    private ArrayList<Integer> foodIds;
    private OrderDao orderDao;
    private IO billIO;

    public Bill(Restaurant restaurant) {
        this.total = 0d;
        this.restaurant = restaurant;
        this.orderDao = new OrderDao();
        this.billIO = new IO();
    }

    public ArrayList<Food> getOrderedFoods() {
        return orderedFoods;
    }

    public double getTotal() {
        this.orderedFoods.forEach(food -> total += food.getPrice());
        return total;
    }

    public void addToBill(Integer foodId) {
        this.foodIds.add(foodId);
    }

    public String getFoodNamesFromBill(){
        ArrayList<String> foodNames = new ArrayList<>();
        orderedFoods.forEach(food -> foodNames.add(food.getName()));
        return String.join(", ", foodNames);
    }

    public ArrayList<Food> sendOrderToKitchen() {
        this.orderedFoods = restaurant.getKitchen().cookFood(foodIds);
        return orderedFoods;
    }

    public void addInRepository(){
        orderDao.createOrder(new Order(this));
    }

    public void printBillDetails() {
        orderedFoods.forEach(food -> System.out.println(food.getName() + " : " + food.getPrice()));
        billIO.printText("Total : " + getTotal());
    }



    public boolean verifyIfPaid() {
        ArrayList<Boolean> checks = new ArrayList<>();
        checks.add(orderedFoods == null);
        checks.add(total == 0d);
        checks.add(foodIds == null);
        return !checks.contains(true);
    }

    public void resetBill() {
        this.orderedFoods = null;
        this.total = 0d;
        this.foodIds = null;
    }
}
