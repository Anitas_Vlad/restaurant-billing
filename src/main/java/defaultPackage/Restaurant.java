package defaultPackage;

import database.dao.FoodDao;
import database.entity.Food;
import exceptions.CannotEnterTheRestaurant;
import exceptions.CannotLeaveTheRestaurant;
import exceptions.CannotSitAtTable;

import java.util.*;

public class Restaurant {

    private final String restaurantName;
    private final Kitchen kitchen;
    private ArrayList<Table> tables;
    private final Set<Person> peopleCurrentlyInsideTheRestaurant;
    private Integer numberOfAvailableTables;
    private FoodDao foodDao;
    private IO restaurantIO;

    public Restaurant(String restaurantName, Kitchen kitchen) {
        this.restaurantName = restaurantName;
        this.kitchen = kitchen;
        this.peopleCurrentlyInsideTheRestaurant = new HashSet<>();
        this.foodDao = new FoodDao();
        this.restaurantIO = new IO();
    }

    public void setTables(ArrayList<Table> tables) {
        this.tables = tables;
        this.numberOfAvailableTables = Math.toIntExact(tables.stream().filter(table -> !table.isOccupied()).count());
    }

    public Kitchen getKitchen() {
        return kitchen;
    }

    public Integer getNumberOfAvailableTables() {
        return numberOfAvailableTables;
    }

    public void showAvailableTables() {
        tables.forEach(table -> {
            if (!table.isOccupied()){
                restaurantIO.printText("Table number " + table.getNumber() + " is available");
            }
        });
    }

    public void acceptPerson(Person person) throws CannotEnterTheRestaurant {
        if (peopleCurrentlyInsideTheRestaurant.contains(person)) {
            throw new CannotEnterTheRestaurant("This person is already inside " + restaurantName);
        }
        if (numberOfAvailableTables < 1) {
            throw new CannotEnterTheRestaurant("No free tables");
        }
        peopleCurrentlyInsideTheRestaurant.add(person);
        restaurantIO.printText("Welcome, " + person.getName() + "!");
    }


    public void acceptAtTable(Person person) throws CannotSitAtTable {
        Scanner scan = new Scanner(System.in);
        if (!peopleCurrentlyInsideTheRestaurant.contains(person)) {
            throw new CannotSitAtTable(person.getName() + " isn't inside the restaurant, therefore cannot sit at table");
        }
        System.out.println(person.getName() + ", please choose a table");
        this.showAvailableTables();
        int desiredTable = scan.nextInt();

        try {
            if (tables.get(desiredTable).isOccupied()) {
                System.out.println("This table is already occupied");
                this.acceptAtTable(person); //Recursive Call
            }
            System.out.println(person.getName() + ", you are enjoying our table" + desiredTable);
            tables.get(desiredTable).occupy(person);
            numberOfAvailableTables = Math.toIntExact(tables.stream().filter(table -> !table.isOccupied()).count());

        } catch (IndexOutOfBoundsException e) {
            throw new CannotSitAtTable("This table does not exist");
        }
    }

    public void acceptAtTableV2(Person person, Table table) throws CannotSitAtTable {
        if (!peopleCurrentlyInsideTheRestaurant.contains(person)) {
            throw new CannotSitAtTable(person.getName() + " isn't inside the restaurant, therefore cannot sit at table");
        }
        if (!tables.contains(table)) {
            throw new CannotSitAtTable("This table is not inside this restaurant");
        }
        try {
            if (table.isOccupied()) {
                System.out.println("This table is already occupied");
                this.acceptAtTable(person); //Recursive Call
            }
            table.occupy(person);
            numberOfAvailableTables = Math.toIntExact(tables.stream().filter(t -> !t.isOccupied()).count());

        } catch (IndexOutOfBoundsException e) {
            throw new CannotSitAtTable("This table does not exist");
        }
    }

    public void ejectPersonFromTable(Person person) throws CannotLeaveTheRestaurant { //TODO Guard Clause
        if (!peopleCurrentlyInsideTheRestaurant.contains(person)) {
            throw new CannotLeaveTheRestaurant(person.getName() + " was not inside this restaurant");
        }
        Table table = (Table) tables.stream().filter(t -> t.getCurrentPerson() == person);

        if (!table.getBill().verifyIfPaid()) {
            throw new CannotLeaveTheRestaurant("Sir, you have not paid yet");
        }
        table.setOccupied(false);
    }

    public void ejectPersonFromRestaurant(Person person) throws CannotLeaveTheRestaurant {
        if (peopleCurrentlyInsideTheRestaurant.contains(person)) {
            throw new CannotLeaveTheRestaurant(person.getName() + " was not inside " + restaurantName);
        }
        numberOfAvailableTables++;
        System.out.println(person.getName() + " has left " + restaurantName);
        peopleCurrentlyInsideTheRestaurant.remove(person);
    }

    public List<Food> showMenu() {
        return foodDao.showFoodMenu();
    }
}
