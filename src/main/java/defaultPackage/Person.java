package defaultPackage;

import exceptions.CannotEnterTheRestaurant;
import exceptions.CannotLeaveTheRestaurant;
import exceptions.CannotPay;
import exceptions.CannotSitAtTable;

public class Person {

    private String name;
    private double availableMoney;

    public Person(String name, double availableMoney) {
        this.name = name;
        this.availableMoney = availableMoney;
    }

    public String getName() {
        return name;
    }

    public double getAvailableMoney() {
        return availableMoney;
    }

    public void enterRestaurant(Restaurant restaurant) throws CannotEnterTheRestaurant {
            restaurant.acceptPerson(this);
    }

    public void sitAtTable(Restaurant restaurant) throws CannotSitAtTable {
        restaurant.acceptAtTable(this);
    }

    public void sitAtTableV2(Restaurant restaurant, Table table) throws CannotSitAtTable { //TODO ideas.txt.input
        restaurant.acceptAtTableV2(this, table);
    }

    public void leaveTable(Restaurant restaurant) throws CannotLeaveTheRestaurant {
        leaveRestaurant(restaurant);
    }

    public void leaveRestaurant(Restaurant restaurant) throws CannotLeaveTheRestaurant {
        restaurant.ejectPersonFromRestaurant(this);
    }

    public void pay(Bill bill) throws CannotPay { //TODO must specify it is a bill the person must pay

        double price = bill.getTotal();
        if (availableMoney < price) {
            throw new CannotPay("Not enough money!");
        }
        this.availableMoney -= price;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name= " + name + '\'' +
                '}';
    }
}
