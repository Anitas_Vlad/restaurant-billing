package database.dao;

import database.entity.Order;
import database.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class OrderDao {
    public void createOrder(Order order){
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(order);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public List<Order> showAllOrders() {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query<Order> query = session.createQuery("select o from Order o", Order.class);
            return query.list();

        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }
}
