package database.dao;

import database.entity.Food;
import database.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class FoodDao {

    public void createFood(Food food) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(food);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public void updateFood(Food food) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(food);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public void deleteFood(Food food) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(food);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public Food getFood(Integer id){
        try{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Food food = session.find(Food.class, id);
            return food;
        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Food> showFoodMenu() {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query<Food> query = session.createQuery("select f from Food f", Food.class);
            return query.list();

        } catch (HibernateException e) {
            e.printStackTrace();
            return null;
        }
    }

}
