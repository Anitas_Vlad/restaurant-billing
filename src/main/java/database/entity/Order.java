package database.entity;

import defaultPackage.Bill;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "date")
    private Date date;
    @Column(name = "price")
    private Double price;
    @Column(name = "description")
    private String description;

    public Order(Bill bill) {
        this.date = Date.from(Instant.now());
        this.price = bill.getTotal();
        this.description = bill.getFoodNamesFromBill();
    }

    public Order(Date date, Double price, String description) {
        this.date = date;
        this.price = price;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", date=" + date +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
