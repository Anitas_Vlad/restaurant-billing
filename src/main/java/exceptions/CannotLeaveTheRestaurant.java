package exceptions;

public class CannotLeaveTheRestaurant extends Exception{
    public CannotLeaveTheRestaurant(String message){
        super(message);
    }
}
