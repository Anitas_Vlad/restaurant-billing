package exceptions;

public class CannotSitAtTable extends RuntimeException {
    public CannotSitAtTable(String message) {
        super(message);
    }
}
