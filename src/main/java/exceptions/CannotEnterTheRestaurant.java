package exceptions;

public class CannotEnterTheRestaurant extends Exception {
    public CannotEnterTheRestaurant(String message) {
        super(message);
    }
}
