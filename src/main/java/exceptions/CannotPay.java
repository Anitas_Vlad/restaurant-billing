package exceptions;

public class CannotPay extends Exception{
    public CannotPay(String message){
        super(message);
    }
}
